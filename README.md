# Lambda Demo #
Example of running Lambda locally.

## Requirements: ##
SAM CLI installed

## Running: ##
Run all commands from within the *examples* directory. To run the index.js file with Lambda, use the following command:

```
sam local invoke test -e test-events/get.json
```

where get.json is the operation and can be substituted with other test-event files.

## Debugging ##
Add the object from *Configurations* array in __launch-EXAMPLE.json to the launch.json file in your VS Code .vscode directory

Launch debugging SAM with the debug flag:

```
sam local invoke test -e test-events/get.json --debug-port 5858
```

When the script is listening for the debugger, select the _Lambda Demo_ option and click the play button connect the debugger