
exports.handler = async (event) => {
    /* 
    This code gets executed on sam local invoke with this command, 
    where the test event file is the operation:
    
    sam local invoke test -e test-events/get.json
    
    Whatever data is available in the test event is what will show in the event"
    */
   console.log(event);

   /**
    * Typically test event files match up to operations in Lambda
    * For example, if this Lambda function was CRUD operations on a db:
    */

    if(event.operation=="get"){ //corrisponds to the get.json test event
        console.log("I'm getting all data");
    }
    if(event.operation=="write"){ //corrisponds to the write.json test event
        console.log("Writing the following data:");
        console.log(event.data)
    }

}